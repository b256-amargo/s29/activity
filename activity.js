
// Finding users with "s" on first name OR "d" in last name
db.users.find( 

	{$or: 
		[
			{firstName: {$regex: "s", $options: "$i"}},
			{lastName: {$regex: "d", $options: "$i"}}
		]
	}, 

	{firstName: 1, lastName: 1, _id: 0} 
);

// Finding users who are from HR department AND has age >= 70
db.users.find({$and: [{department: "HR"}, {age: {$gte: 70}}] });

// Finding users with "e" on first name AND has age <= 30
db.users.find({$and: [{firstName: {$regex: "e"}}, {age: {$lte: 30}}] });